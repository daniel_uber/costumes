# Costumes

This project is designed to present a web application to allow costume shop staff
to access, add, and update actor measurements at fittings. This should give a head start
for costumers before a first visit for measurements can be made, and streamline the cases where
nothing changed.

* Access to be by invitation only, Login required.
* Designed to be shared with your team, and your peers at other theaters.
* Allows search by name, and adding actor details as you need them.

Currently hosted at [bitbucket](https://bitbucket.org/daniel_uber/costumes) and staging at [costumes.djuber.tk](https://costumes.djuber.tk)