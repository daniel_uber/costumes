from django.conf.urls import url

from . import views

app_name = 'actors'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^add/$', views.add_actor, name='add_actor'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='actor'),
    url(r'^(?P<actor_id>[0-9]+)/measurements/$', views.measurements, name='measurements'),
]
