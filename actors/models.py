from django.db import models
from datetime import date
import django.utils.timezone
from django.core import validators
# Create your models here.

class Actor(models.Model):
    first_name = models.CharField(max_length=100, blank=False, null=False)
    last_name = models.CharField(max_length=100, blank=False, null=False)
    phone_number = models.CharField(max_length=20,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)
    
class Recorder(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20,null=True)
    email = models.EmailField(null=True)


# https://docs.djangoproject.com/en/1.10/topics/db/models/
# default
#    The default value for the field. This can be a value or a callable object.
# If callable it will be called every time a new object is created.
    
class Measurements(models.Model):
    actor = models.ForeignKey(Actor, on_delete=models.CASCADE)
    #taken_by = models.ForeignKey(Recorder, on_delete=models.NULL=TRUE)
    date_taken = models.DateField(default=django.utils.timezone.now)
    around_head = models.FloatField(blank=True, default=0.0)
    ear_to_ear = models.FloatField(blank=True, default=0.0)
    nape_to_hairline = models.FloatField(blank=True, default=0.0)
    neck = models.FloatField(blank=True, default=0.0)
    neck_base = models.FloatField(blank=True, default=0.0)
    chest_bust = models.FloatField(blank=True, default=0.0)
    ribcage = models.FloatField(blank=True, default=0.0)
    tip_tip = models.FloatField(blank=True, default=0.0)
    tip_tip_around_neck = models.FloatField(blank=True, default=0.0)
    with_bra = models.BooleanField(blank=True, default=False)
    waist = models.FloatField(blank=True, default=0.0)
    hi_hip = models.FloatField(blank=True, default=0.0)
    hi_hip_at = models.FloatField(blank=True, default=0.0)
    low_hip = models.FloatField(blank=True, default=0.0)
    low_hip_at = models.FloatField(blank=True, default=0.0)
    cf_to_waist = models.FloatField(blank=True, default=0.0)
    cf_to_floor = models.FloatField(blank=True, default=0.0)
    shoulders_x_front = models.FloatField(blank=True, default=0.0)
    armscyes_x_front = models.FloatField(blank=True, default=0.0)
    cb_to_waist = models.FloatField(blank=True, default=0.0)
    jacket_length = models.FloatField(blank=True, default=0.0)
    cb_to_floor = models.FloatField(blank=True, default=0.0)
    shoulders_x_back = models.FloatField(blank=True, default=0.0)
    armscyes_x_back = models.FloatField(blank=True, default=0.0)
    half_girth = models.FloatField(blank=True, default=0.0)
    full_girth = models.FloatField(blank=True, default=0.0)
    nape = models.FloatField(blank=True, default=0.0)
    shoulder_seam = models.FloatField(blank=True, default=0.0)
    shoulder_to_elbow = models.FloatField(blank=True, default=0.0)
    shoulder_to_wrist = models.FloatField(blank=True, default=0.0)
    nape_to_wrist  = models.FloatField(blank=True, default=0.0)
    underarm_straight = models.FloatField(blank=True, default=0.0)
    underarm_to_waist = models.FloatField(blank=True, default=0.0)
    armscye = models.FloatField(blank=True, default=0.0)
    right_handed = models.BooleanField(blank=True, default=True)
    bicep  = models.FloatField(blank=True, default=0.0)
    forearm = models.FloatField(blank=True, default=0.0)
    wrist = models.FloatField(blank=True, default=0.0)
    thigh = models.FloatField(blank=True, default=0.0)
    below_knee = models.FloatField(blank=True, default=0.0)
    calf = models.FloatField(blank=True, default=0.0)
    ankle = models.FloatField(blank=True, default=0.0)
    inseam_to_below_knee = models.FloatField(blank=True, default=0.0)
    inseam_to_floor = models.FloatField(blank=True, default=0.0)
    side_waist_to_below_knee = models.FloatField(blank=True, default=0.0)
    side_waist_to_floor = models.FloatField(blank=True, default=0.0)
    # height may want to be expressed as 5'10" rather than 70.0?
    height = models.FloatField(blank=True, default=0.0)
    weight = models.FloatField(blank=True, default=0.0)
    suit_dress_size = models.CharField(blank=True, null=True, max_length=32)
    bra_size = models.CharField(blank=True, null=True, max_length=32)
    shirt_size = models.CharField(blank=True, null=True, max_length=32)
    pants_size = models.CharField(blank=True, null=True, max_length=32)
    tights_size = models.CharField(blank=True, null=True, max_length=32)
    shoe_size = models.CharField(blank=True, null=True, max_length=32)
    hat_size = models.CharField(blank=True, null=True, max_length=32)
    glove_size = models.CharField(blank=True, null=True, max_length=32)
    pierced_ears = models.BooleanField(blank=True, default=True)
    hair_color  = models.CharField(max_length=32)
    ring_size_band = models.CharField(blank=True, null=True, max_length=32)
    allergies = models.TextField(blank=True, null=True)
    tattoos = models.TextField(blank=True, null=True)
    figure_notes = models.CharField( blank=True, null=True, max_length=255)
    
    
    
    
 
