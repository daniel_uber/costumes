from django.test import TestCase
from actors.models import Actor
from django.core.exceptions import ValidationError

# Create your tests here.

class ActorTests(TestCase):
    
    def test_str(self):
        actor=Actor(first_name='Dan', last_name='Uber')

        self.assertEquals(str(actor),'Dan Uber')
        
    def test_defaults(self):
        actor=Actor(first_name='Boris', last_name='Johnson')
        self.assertEqual(actor.phone_number, None)
        self.assertEqual(actor.email, None)

    def test_required_name(self):
        anActor = Actor(first_name='Floyd', email='floyd@gmail.com')
        # null last name not allowed:
        self.assertRaises(ValidationError, anActor.full_clean)
        anActor.last_name='Barber'
        anActor.full_clean() # should not raise
        anActor.first_name=''
        # blank first name not allowed:
        self.assertRaises(ValidationError, anActor.full_clean)
        
    def test_email_validation(self):
        anActor = Actor(first_name = 'Floyd', last_name='Barber', email='floyd.barber')
        self.assertRaises(ValidationError, anActor.full_clean)
        anActor.email = 'floyd@barber.me'
        # should not raise
        anActor.full_clean()
        
