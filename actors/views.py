from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic

from .models import Actor

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'actors/index.html'
    context_object_name = 'actor_list'

    def get_queryset(self):
        return Actor.objects.order_by('last_name');


class DetailView(generic.DetailView):
    model = Actor
    template_name = 'actors/actor.html'
    context_object_name = 'actor'

    

def index(request):
    actor_list = Actor.objects.order_by('last_name')
    context = {'actor_list': actor_list}
    return render(request, 'actors/index.html', context)

def actor(request, actor_id):
    return HttpResponse("You're looking at actor %s" % actor_id)

def measurements(request, actor_id):
    return HttpResponse("You're looking at measurements for actor %s" % actor_id)

def add_actor(request):
    return HttpResponse("You want to add an actor")
