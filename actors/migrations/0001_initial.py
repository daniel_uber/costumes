# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-28 07:38
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('phone_number', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Measurements',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_taken', models.DateField()),
                ('around_head', models.FloatField()),
                ('ear_to_ear', models.FloatField()),
                ('nape_to_hairline', models.FloatField()),
                ('neck', models.FloatField()),
                ('neck_base', models.FloatField()),
                ('chest_bust', models.FloatField()),
                ('ribcage', models.FloatField()),
                ('tip_tip', models.FloatField()),
                ('tip_tip_around_neck', models.FloatField()),
                ('with_bra', models.BooleanField()),
                ('waist', models.FloatField()),
                ('hi_hip', models.FloatField()),
                ('hi_hip_at', models.FloatField()),
                ('low_hip', models.FloatField()),
                ('low_hip_at', models.FloatField()),
                ('cf_to_waist', models.FloatField()),
                ('cf_to_floor', models.FloatField()),
                ('shoulders_x_front', models.FloatField()),
                ('armscyes_x_front', models.FloatField()),
                ('cb_to_waist', models.FloatField()),
                ('jacket_length', models.FloatField()),
                ('cb_to_floor', models.FloatField()),
                ('shoulders_x_back', models.FloatField()),
                ('armscyes_x_back', models.FloatField()),
                ('half_girth', models.FloatField()),
                ('full_girth', models.FloatField()),
                ('nape', models.FloatField()),
                ('shoulder_seam', models.FloatField()),
                ('shoulder_to_elbow', models.FloatField()),
                ('shoulder_to_wrist', models.FloatField()),
                ('nape_to_wrist', models.FloatField()),
                ('underarm_straight', models.FloatField()),
                ('underarm_to_waist', models.FloatField()),
                ('armscye', models.FloatField()),
                ('right_handed', models.BooleanField()),
                ('bicep', models.FloatField()),
                ('forearm', models.FloatField()),
                ('wrist', models.FloatField()),
                ('thigh', models.FloatField()),
                ('below_knee', models.FloatField()),
                ('calf', models.FloatField()),
                ('ankle', models.FloatField()),
                ('inseam_to_below_knee', models.FloatField()),
                ('inseam_to_floor', models.FloatField()),
                ('side_waist_to_below_knee', models.FloatField()),
                ('side_waist_to_floor', models.FloatField()),
                ('height', models.FloatField()),
                ('weight', models.FloatField()),
                ('suit_dress_size', models.CharField(max_length=32)),
                ('bra_size', models.CharField(max_length=32)),
                ('shirt_size', models.CharField(max_length=32)),
                ('pants_size', models.CharField(max_length=32)),
                ('tights_size', models.CharField(max_length=32)),
                ('shoe_size', models.CharField(max_length=32)),
                ('hat_size', models.CharField(max_length=32)),
                ('glove_size', models.CharField(max_length=32)),
                ('pierced_ears', models.BooleanField()),
                ('hair_color', models.CharField(max_length=32)),
                ('ring_size_band', models.CharField(max_length=32)),
                ('allergies', models.TextField()),
                ('tattoos', models.TextField()),
                ('figure_notes', models.CharField(max_length=200)),
                ('actor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='actors.Actor')),
            ],
        ),
        migrations.CreateModel(
            name='Recorder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('phone_number', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
    ]
